const Crawler = require("crawler");
const db = require('mysql-ithm')
db.connect({
    host: 'localhost',//数据库地址
    port: '3306',
    user: 'root',//用户名，没有可不填
    password: 'root',//密码，没有可不填
    database: 'vido'//数据库名称
});
let heroModel = db.model('hero', {
    heroName: String,
    heroSkill: String,
    heroIcon: String,
    isDelete: String
});
var c = new Crawler({
    maxConnections: 10,
    // This will be called for each crawled page
    callback: function (error, res, done) {
        if (error) {
            console.log(error);
        } else {
            var $ = res.$;

            JSON.parse(res.body).forEach(element => {
                // Queue just one URL, with default callback
                let url = `https://pvp.qq.com/web201605/herodetail/${element.ename}.shtml`
                xq.queue(url);
            });
        }
        done();
    }
});

// Queue just one URL, with default callback
c.queue('https://pvp.qq.com/web201605/js/herolist.json');
var heros = []
var xq = new Crawler({
    maxConnections: 10,
    // This will be called for each crawled page
    callback: function (error, res, done) {
        if (error) {
            console.log(error);
        } else {
            var $ = res.$;
            heros.push({
                heroName: $('.cover-name').text(),
                heroSkill: $('.skill-name>b').first().text(),
                heroIcon: 'http:' + $('.ico-play').prev().attr('src')
            })
        }
        done();
    }
});
//要等待所有的请求全部做完之后，才入库
xq.on('drain', function () {
    heroModel.insert(heros, (err, result) => {
        if (err == null) {
            console.log('添加成功')
        } else {
            console.log('添加失败')
        }
    })
});

