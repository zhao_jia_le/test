// 1.导包
const express = require('express')
const cors = require('cors')
const db = require('mysql-ithm')
const multer = require('multer')
const bodyParser = require('body-parser')
const svgCaptcha = require('svg-captcha');
const cookieSession = require('cookie-session')

var upload = multer({ dest: 'uploads/' })
db.connect({
    host: 'localhost',//数据库地址
    port: '3306',
    user: 'root',//用户名，没有可不填
    password: 'root',//密码，没有可不填
    database: 'vido'//数据库名称
});
let heroModel = db.model('hero', {
    heroName: String,
    heroSkill: String,
    heroIcon: String,
    isDelete: String
});
let userModel = db.model('user', {
    username: String,
    password: String
})
// 2.设置服务器
const app = express()
// 3.托管静态资源
app.use(express.static('www'))
app.use(express.static('uploads'))
// 4.设置中间件
app.use(cors())
app.use(bodyParser.urlencoded({ extended: false }))
// cooike的中间件
app.use(cookieSession({
    name: 'session',//cooike的名字
    keys: ['niah', 'fjdsj', 'dfs'],//加密的加盐

    // Cookie Options   24小时候过期
    maxAge: 24 * 60 * 60 * 1000 // 24 hours
}))
// 5.写接口
// 5.0判断是否登录
app.get('/isLogin', (req, res) => {
    res.send(req.session.user)
})
// 5.1查询所有英雄
app.get('/hero/list', (req, res) => {
    //查询当前的cookie
    //如果没有登录，直接来访问这个list接口，那这个cookie就是undefined.
    //如果登录了，来访问这个list页面，那这个cookie就是有值的.
    //那现在我们就可以判断这个有没有值,从而判断你有没有登录，
    /**
     * 结果：
     * 有cooike：{ username: 'zjy', password: '5137e8337b008c87b11bb7e12007f142' }
     * 无cooike：undefined
     * */
    // console.log(req.session.user)

    let { search } = req.query

    if (!search) {
        // undefined
        heroModel.find('isDelete="false"', (err, results) => {
            if (err == null) {
                res.send({
                    code: 200,
                    heros: results,

                })
            } else {
                res.send({
                    code: 500,
                    msg: '服务器错误'
                })
            }
        });
    } else {
        // 有参数---通过模糊查询把所有包含该字的字段全部查出来(不包含isdelete为true)
        // #6:关键字like模糊查询，模糊匹配，可以结合通配符来使用
        // select * from employee where name like 'eg%';
        heroModel.find(`heroName like '%${search}%' and isDelete ="false"`, (err, results) => {
            if (err == null) {
                res.send({
                    code: 200,
                    heros: results,

                })
            } else {
                res.send({
                    code: 500,
                    msg: '服务器错误'
                })
            }
        })
    }

})
// 5.2根据id查英雄信息
app.get('/hero/info', (req, res) => {
    let { id } = req.query
    heroModel.find(`id=${id}`, (err, results) => {
        if (err == null) {
            res.send({
                code: 200,
                data: results[0]
            })
        } else {
            res.send({
                code: 500,
                msg: '服务器错误'
            })
        }
    })
})
// 5.3编辑英雄
app.post('/hero/update', upload.single('heroIcon'), (req, res) => {
    // 接受到前端传过来的参数
    let { id, heroName, heroSkill } = req.body
    // 把参数封装成一个对象
    let obj = {
        heroName,
        heroSkill
    }
    // 证明有传图片就添加到对象中,没有就不添加证明不用改
    if (req.file != undefined) {
        obj.heroIcon = 'http:127.0.0.1:4399/' + req.file.filename
    }
    heroModel.update(`id=${id}`, obj, (err, results) => {
        if (err == null) {
            res.send({
                code: 200,
                msg: '新增成功'
            })
        } else {
            res.send({
                code: 500,
                msg: '服务器错误'
            })
        }
    })


})
// 5.4删除英雄(软删除)
app.post('/hero/delete', (req, res) => {
    let { id } = req.body
    heroModel.update(`id=${id}`, { isDelete: 'true' }, (err, results) => {
        if (err == null) {
            res.send({
                code: 200,
                msg: '删除成功'
            })
        } else {
            res.send({
                code: 500,
                msg: '服务器错误'
            })
        }
    })
})
// 5.5新增英雄
app.post('/hero/add', upload.single('heroIcon'), (req, res) => {
    let { heroName, heroSkill } = req.body
    let heroIcon = 'http://127.0.0.1:4399/' + req.file.filename
    heroModel.insert({ heroName, heroSkill, heroIcon, isDelete: 'false' }, (err, results) => {
        if (err == null) {
            res.send({
                code: 200,
                msg: '新增成功'
            })
        } else {
            res.send({
                code: 500,
                msg: '服务器错误'
            })
        }
    })
})
// 5.6验证码
let captchaText = '';
app.get('/captcha', function (req, res) {
    var captcha = svgCaptcha.create({
        size: 5,
        ignoreChars: 'oi1l',
        noise: 3,
        color: true,
        background: '#cc9966'
    });
    captchaText = captcha.text

    res.type('svg');
    res.status(200).send(captcha.data);
});
// 5.7用户注册
app.post('/hero/register', (req, res) => {
    let { username, password, code } = req.body
    if (code.toLocaleLowerCase() != captchaText.toLocaleLowerCase()) {
        // 验证码不正确
        res.send({
            code: 402,
            msg: '验证码错误'
        })
    } else {
        // 验证码正确
        userModel.find(`username="${username}"`, (err, results) => {
            if (err == null) {
                if (results.length > 0) {
                    // 用户存在
                    res.send({
                        code: 401,
                        msg: '用户名已存在'
                    })
                } else {
                    userModel.insert({ username, password }, (err, results) => {
                        if (err == null) {
                            res.send({
                                code: 200,
                                msg: '注册成功'
                            })
                        } else {
                            res.send({
                                code: 500,
                                msg: '服务器错误'
                            })
                        }
                    })
                }

            } else {
                res.send({
                    code: 500,
                    msg: '服务器错误'
                })
            }
        });
    }
})
// 5.8用户登录
app.post('/user/login', (req, res) => {
    let { username, password } = req.body
    userModel.find(`username="${username}" and password="${password}"`, (err, results) => {
        if (err == null) {
            if (results.length > 0) {
                // 设置cooike
                req.session.user = { username, password };
                res.send({
                    code: 200,
                    msg: '登录成功'
                })
            } else {
                res.send({
                    code: 401,
                    msg: '账号或密码错误'
                })
            }
        } else {
            res.send({
                code: 500,
                msg: '服务器错误'
            })
        }
    })

})
//5.9退出登录
app.get('/logout', (req, res) => {
    // 清除cooike
    req.session = null
    // 重定向
    res.writeHead(302, {
        'Location': './login.html'
    })
    res.end()
})


// 6.开启服务器
app.listen(4399, () => {
    console.log('服务器开启了~~~')
})